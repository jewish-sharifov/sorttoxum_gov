{extends file="base.tpl"}

{block name="page-title"}
	:: {$messages.common.p404}
{/block}

{block name="content"}
	<div class="plants_seeds_block">
		<div class="plants_seeds_content_block_left_about" style="width: 100% !important;">
			<h1>404 page not found</h1>
		</div>
		<div class="clear-both"></div>
	</div>
{/block}